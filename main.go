package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/trafficmanager/orchestration"
	"github.com/trafficmanager/types"
)

func main() {
	N := &types.TrafficLight{
		Title: "N",
	}

	S := &types.TrafficLight{
		Title: "S",
	}

	W := &types.TrafficLight{
		Title: "W",
	}

	E := &types.TrafficLight{
		Title: "E",
	}

	g1, err := types.NewLightsGroup(N, S)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	g2, err := types.NewLightsGroup(W, E)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	settings := make(map[types.Color]time.Duration)
	settings[types.Red] = time.Minute * 5
	settings[types.Green] = time.Minute * 5
	settings[types.Yellow] = time.Second * 30

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Description:")
	fmt.Println("Given roads intersection with 4 traffic lights working in pairs.\nApplication calculates each light (Red, Yellow and Green) duration for entered period for each pair and prints a map.\nGiven Green and Red lights duration is 5 minutes and Yellow is 30 seconds.")
	fmt.Print("Please enter simulation period in minutes [default: 30]: ")

	period := 30
	for {
		inText, _ := reader.ReadString('\n')
		inText = strings.Replace(inText, "\n", "", -1)
		if inText != "" {
			period, err = strconv.Atoi(inText)
			if err != nil {
				fmt.Printf("Error: %s\n", err.Error())
				return
			}
		}

		break
	}

	manager, err := orchestration.NewTrafficManager(settings)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	results, err := manager.GenerateMap(time.Minute*time.Duration(period), []*types.LightsGroup{g1, g2})
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	if results != nil && len(results) > 0 {
		printResults(results, g1, g2)
	} else {
		fmt.Println("Results variable contains empty result set or nil")
	}
}

//printResults prints results set for given TrafficLight`s groups
func printResults(results []*types.IntersectionState, groups ...*types.LightsGroup) {

	red := color.New(color.FgRed)
	yellow := color.New(color.FgYellow)
	green := color.New(color.FgGreen)

	for _, group := range groups {
		for _, signal := range group.Lights {
			fmt.Printf("%s ", signal.Title)
		}
	}

	var totalDuration time.Duration
	fmt.Println()
	for _, result := range results {
		totalDuration += result.Duration
		for _, state := range result.Items {
			switch state {
			case types.Red:
				red.Print("R ")
				break
			case types.Yellow:
				yellow.Print("Y ")
				break
			case types.Green:
				green.Print("G ")
				break
			}
		}
		fmt.Printf("%.1f(min)\n", result.Duration.Minutes())
	}
	fmt.Println("-----------")
	fmt.Printf("Total: %.1f(min)\n", totalDuration.Minutes())
}
