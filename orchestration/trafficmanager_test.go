package orchestration

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/trafficmanager/types"
)

func TestNewTrafficManager(t *testing.T) {

	settings := make(map[types.Color]time.Duration)
	t.Run("NilError", func(t *testing.T) {
		manager, err := NewTrafficManager(nil)
		assert.Error(t, err)
		assert.Nil(t, manager)
	})
	t.Run("EmptySettings", func(t *testing.T) {
		manager, err := NewTrafficManager(settings)
		assert.Error(t, err)
		assert.Nil(t, manager)
	})
	t.Run("IncompleteSettings", func(t *testing.T) {
		settings[types.Red] = 10
		settings[types.Yellow] = 10
		manager, err := NewTrafficManager(settings)
		assert.Error(t, err)
		assert.Nil(t, manager)
	})
	t.Run("Success", func(t *testing.T) {
		settings[types.Green] = 10
		manager, err := NewTrafficManager(settings)
		assert.NoError(t, err)
		assert.NotNil(t, manager)
		assert.Equal(t, settings, manager.settings)
	})
}

func TestGenerateMap(t *testing.T) {
	settings := make(map[types.Color]time.Duration)
	settings[types.Red] = time.Duration(time.Minute * 5)
	settings[types.Yellow] = time.Duration(time.Second * 30)
	settings[types.Green] = time.Duration(time.Minute * 5)

	manager, err := NewTrafficManager(settings)
	assert.NoError(t, err)
	assert.NotNil(t, manager)

	t.Run("NoDuration", func(t *testing.T) {
		_, err := manager.GenerateMap(0, nil)
		assert.Error(t, err)
	})
	t.Run("NoLightGroups", func(t *testing.T) {
		_, err := manager.GenerateMap(10, nil)
		assert.Error(t, err)
	})
	t.Run("EmptyGroup", func(t *testing.T) {
		emptyGroup := types.LightsGroup{}
		_, err := manager.GenerateMap(30, []*types.LightsGroup{&emptyGroup})
		assert.Error(t, err)
	})
	t.Run("Success", func(t *testing.T) {
		g1, err := types.NewLightsGroup(&types.TrafficLight{"A"})
		assert.NoError(t, err)
		assert.NotNil(t, g1)

		g2, err := types.NewLightsGroup(&types.TrafficLight{"B"})
		assert.NoError(t, err)
		assert.NotNil(t, g2)

		results, err := manager.GenerateMap(time.Duration(time.Minute*30), []*types.LightsGroup{g1, g2})
		assert.NoError(t, err)
		assert.NotNil(t, results)
		assert.Len(t, results, 15)

		assert.Equal(t, time.Duration(time.Second*30), results[0].Duration)
		assert.Equal(t, []types.Color{types.Yellow, types.Red}, results[0].Items)
		assert.Equal(t, time.Duration(time.Minute*5), results[7].Duration)
		assert.Equal(t, []types.Color{types.Green, types.Red}, results[7].Items)
		assert.Equal(t, time.Duration(time.Second*30), results[14].Duration)
		assert.Equal(t, []types.Color{types.Yellow, types.Red}, results[14].Items)
	})
}

func TestGroupsToReportRow(t *testing.T) {
	g1, err := types.NewLightsGroup(&types.TrafficLight{"A"})
	assert.NoError(t, err)
	assert.NotNil(t, g1)
	g1.CurrentColor = types.Red

	g2, err := types.NewLightsGroup(&types.TrafficLight{"B"})
	assert.NoError(t, err)
	assert.NotNil(t, g2)
	g2.CurrentColor = types.Green

	settings := make(map[types.Color]time.Duration)
	settings[types.Red] = time.Duration(time.Minute * 5)
	settings[types.Yellow] = time.Duration(time.Second * 30)
	settings[types.Green] = time.Duration(time.Minute * 5)

	manager, err := NewTrafficManager(settings)
	assert.NoError(t, err)
	assert.NotNil(t, manager)

	state := manager.groupsToReportRow([]*types.LightsGroup{g1, g2})
	assert.NotNil(t, state)
	assert.Equal(t, []types.Color{types.Red, types.Green}, state.Items)
	assert.Equal(t, time.Duration(time.Minute*5), state.Duration)
}

func TestIsStateValid(t *testing.T) {
	g1, err := types.NewLightsGroup(&types.TrafficLight{"A"})
	assert.NoError(t, err)
	assert.NotNil(t, g1)
	assert.Equal(t, types.Color(types.Red), g1.CurrentColor)

	g2, err := types.NewLightsGroup(&types.TrafficLight{"B"})
	assert.NoError(t, err)
	assert.NotNil(t, g2)
	assert.Equal(t, types.Color(types.Red), g2.CurrentColor)

	groups := []*types.LightsGroup{g1, g2}

	t.Run("AllRed", func(t *testing.T) {
		assert.True(t, isStateValid(groups))
	})
	t.Run("1Red1Green", func(t *testing.T) {
		g2.CurrentColor = types.Green
		assert.True(t, isStateValid(groups))
	})
	t.Run("AllGreen", func(t *testing.T) {
		g1.CurrentColor = types.Green
		assert.False(t, isStateValid(groups))
	})
}
