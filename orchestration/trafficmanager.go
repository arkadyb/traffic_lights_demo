package orchestration

import (
	"errors"
	"time"

	"github.com/trafficmanager/types"
)

//TrafficManager is used to control a system of traffic lights for given roads intersection
type TrafficManager struct {
	settings map[types.Color]time.Duration
}

//NewTrafficManager creates a new TrafficManager with given settings
func NewTrafficManager(settings map[types.Color]time.Duration) (*TrafficManager, error) {
	if settings == nil {
		return nil, errors.New("Settings cant be nil")
	}

	if _, ok := settings[types.Red]; !ok {
		return nil, errors.New("Settings seem to be incomplete (RED light duration is missing)")
	}
	if _, ok := settings[types.Yellow]; !ok {
		return nil, errors.New("Settings seem to be incomplete (YELLOW light duration is missing)")
	}
	if _, ok := settings[types.Green]; !ok {
		return nil, errors.New("Settings seem to be incomplete (GREEN light duration is missing)")
	}

	m := new(TrafficManager)
	m.settings = settings

	return m, nil
}

//GenerateMap creates a set of IntersectionState objects containing system state for every moment traffic lights switch their colors
func (m *TrafficManager) GenerateMap(duration time.Duration, groups []*types.LightsGroup) ([]*types.IntersectionState, error) {

	var currentDuration time.Duration
	var results []*types.IntersectionState

	if duration == 0 {
		return nil, errors.New("Duration cant be 0")
	}

	if groups == nil || len(groups) == 0 {
		return nil, errors.New("Groups collections cant be nil or empty")
	}

	for {
		for _, group := range groups {

			for i := types.Red; i <= types.Green; i++ {
				if group.Lights == nil || len(group.Lights) == 0 {
					return nil, errors.New("Lights array cant be nil or empty")
				}

				nextColor := group.NextColor()
				if currentDuration+m.settings[nextColor] > duration {
					return results, nil
				}

				group.Switch(nextColor)
				currentDuration += m.settings[nextColor]

				if !isStateValid(groups) {
					group.Reset()
					return nil, errors.New("System is in invalid state. All lights reverted to RED.")
				}

				results = append(results, m.groupsToReportRow(groups))
			}

			//One-signal system does not require to be reset
			if len(groups) > 1 {
				group.Reset()
			}
		}
	}
}

//groupsToReportRow creates IntersectionState object for traffic lights system at given moment
func (m *TrafficManager) groupsToReportRow(groups []*types.LightsGroup) *types.IntersectionState {

	colors := make([]types.Color, 0)
	var duration time.Duration
	for _, group := range groups {
		if group.CurrentColor != types.Red || len(groups) == 1 {
			duration = m.settings[group.CurrentColor]
		}

		for i := 0; i < len(group.Lights); i++ {
			colors = append(colors, group.CurrentColor)
		}
	}

	return types.NewStateObject(colors, duration)
}

//isStateValid verifies traffic lights system is valid
func isStateValid(groups []*types.LightsGroup) bool {

	nonRedFound := false
	for _, group := range groups {
		if group.CurrentColor != types.Red {
			if !nonRedFound {
				nonRedFound = true
			} else {
				return false
			}
		}
	}

	return true
}
