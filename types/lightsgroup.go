package types

import "errors"

//Color object is used in LightsGroup to specify current color for given traffic lights group
type Color int

const (
	//Red is color used in traffic lights group
	Red Color = 1
	//Yellow is color used in traffic lights group
	Yellow = 2
	//Green is color used in traffic lights group
	Green = 3
)

//LightsGroup object represents a set of traffic lights that work together
type LightsGroup struct {
	prevColor Color

	CurrentColor Color
	Lights       []*TrafficLight
}

//NewLightsGroup is used to create a new LightsGroup object representing a set of traffic lights that work together
func NewLightsGroup(lights ...*TrafficLight) (*LightsGroup, error) {
	if lights == nil || len(lights) == 0 {
		return nil, errors.New("Lights parameter cant be nil or empty")
	}

	g := &LightsGroup{
		prevColor:    Red,
		CurrentColor: Red,
	}

	g.Lights = lights

	return g, nil
}

//Reset switches group of traffic light to Red color
func (g *LightsGroup) Reset() {
	g.CurrentColor = Red
	g.prevColor = Red
}

//NextColor returns a next color in chain of Red, Yellow and Green colors for given traffic lights group
func (g *LightsGroup) NextColor() Color {
	if g.CurrentColor == Red || g.CurrentColor == Green {
		return Yellow
	}

	if g.prevColor == Red {
		return Green
	}

	return Red
}

//Switch changes color of traffic lights in given group
func (g *LightsGroup) Switch(color Color) {
	g.CurrentColor = color

	if color != Yellow {
		g.prevColor = color
	}
}
