package types

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewStateObject(t *testing.T) {
	state := NewStateObject([]Color{Red, Yellow, Green}, 5)
	assert.NotNil(t, state)
	assert.Equal(t, time.Duration(5), state.Duration)
	assert.Len(t, state.Items, 3)
}
