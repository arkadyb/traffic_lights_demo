package types

import "time"

//IntersectionState represents a set of colors for roads intersaction in given moment
type IntersectionState struct {
	Items    []Color
	Duration time.Duration
}

//NewStateObject creates a new IntersectionState object
func NewStateObject(items []Color, duration time.Duration) *IntersectionState {

	obj := new(IntersectionState)
	obj.Items = items
	obj.Duration = duration

	return obj
}
