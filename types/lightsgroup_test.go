package types

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewLightsGroup(t *testing.T) {

	tests := []struct {
		name          string
		args          []*TrafficLight
		errorexpected bool
	}{
		{name: "Empty", args: []*TrafficLight{}, errorexpected: true},
		{name: "1 Light", args: []*TrafficLight{{Title: "A"}}},
		{name: "3 Lights", args: []*TrafficLight{{Title: "A"}, {Title: "B"}, {Title: "C"}}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res, err := NewLightsGroup(tt.args...)
			if err != nil && !tt.errorexpected {
				t.Error(err)
			} else if err == nil {
				if len(res.Lights) != len(tt.args) {
					t.Errorf("Expected number of signals collection -%d, differs to actual %d", len(tt.args), len(res.Lights))
				}

				if res.CurrentColor != Red && res.prevColor != Red {
					t.Error("Default signal color should be Red")
				}

				for i := 0; i < len(tt.args); i++ {
					if res.Lights[i] != tt.args[i] {
						t.Errorf("Wrong signals set in created group")
					}
				}
			}

		})
	}

}

func TestRest(t *testing.T) {
	g, err := NewLightsGroup(&TrafficLight{"A"})
	assert.NoError(t, err)
	assert.Equal(t, Red, g.CurrentColor)
	assert.Equal(t, Red, g.prevColor)
}

func TestNextColor(t *testing.T) {
	g, err := NewLightsGroup(&TrafficLight{"A"})
	assert.NoError(t, err)

	assert.Equal(t, Color(Yellow), g.NextColor())

	g.CurrentColor = Yellow
	assert.Equal(t, Color(Green), g.NextColor())

	g.CurrentColor = Yellow
	g.prevColor = Green
	assert.Equal(t, Color(Red), g.NextColor())

}

func TestSwitch(t *testing.T) {
	g, err := NewLightsGroup(&TrafficLight{"A"})
	assert.NoError(t, err)
	assert.Equal(t, Color(Red), g.CurrentColor)
	assert.Equal(t, Color(Red), g.prevColor)

	g.Switch(Green)
	assert.Equal(t, Color(Green), g.CurrentColor)
	assert.Equal(t, Color(Green), g.prevColor)

	g.Switch(Yellow)
	assert.Equal(t, Color(Yellow), g.CurrentColor)
	assert.Equal(t, Color(Green), g.prevColor)
}
